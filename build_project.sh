#!/bin/bash
# lanshark-helpdesk shell script to upload to pypi.

WORKDIR=/tmp/lanshark-helpdesk-build.$$
mkdir $WORKDIR
pushd $WORKDIR

git clone git://gitlab.com/lansharkconsulting/lanshark-helpdesk.git
cd lanshark-helpdesk

/usr/bin/python setup.py sdist upload

popd
rm -rf $WORKDIR
